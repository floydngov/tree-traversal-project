from binary_node import BinaryNode as Node

n1 = Node(value=2)
n2 = Node(value=5)
n3 = Node(value=1, left=n1, right=n2)
n4 = Node(value=7)
n5 = Node(value=3)
n6 = Node(value=6, left=n4, right=n5)
n7 = Node(value=4, left=n3, right=n6)

graph = n7

'''
Traverse the tree to find the minimum value in the tree
'''
def min_of_tree(graph):
    if graph is None:
        return None
    min_val = graph.value
    if graph.left:
        left_min = min_of_tree(graph.left)
        if left_min is not None:
            min_val = min(min_val, left_min)
    if graph.right:
        right_min = min_of_tree(graph.right)
        if right_min is not None:
            min_val = min(min_val, right_min)
    return min_val
'''
Traverse the tree to find the maximum value in the tree
'''
def max_of_tree(graph):
    if graph is None:
        return None
    max_val = graph.value
    if graph.left:
        left_max = max_of_tree(graph.left)
        if left_max is not None:
            max_val = max(max_val, left_max)
    if graph.right:
        right_max = max_of_tree(graph.right)
        if right_max is not None:
            max_val = max(max_val, right_max)
    return max_val

# Pass these tests
assert min_of_tree(graph) == 1
assert max_of_tree(graph) == 7
